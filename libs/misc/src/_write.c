#include <errno.h>
#include "uartLog.h"

// 1. to send imediately: 'setvbuf(stdout, NULL, _IONBF, 0);'
// 2. the end of string must '\n' (for uart "\r\n")

int _write(int file, char *ptr, int len)
{
	switch (file) {
	case STDOUT_FILENO: /* stdout */
		uartDebug(ptr, len);
		break;
	case STDERR_FILENO: /* stderr */
		uartDebug(ptr, len);
		break;
	default:
		errno = EBADF;
		return -1;
	}
	return len;
}
