SOURCES += \
	./libs/misc/src/_read.c \
	./libs/misc/src/_write.c \
	./libs/CMSIS/src/core_cm3.c \
	./libs/CMSIS/src/startup_stm32f10x.c \
	./libs/CMSIS/src/vectors_stm32f10x.c \
	./libs/CMSIS/src/system_stm32f10x.c \
	./libs/CMSIS/src/startup_cm.c \
	./libs/StdPeriph/src/stm32f10x_can.c \
	./libs/StdPeriph/src/stm32f10x_wwdg.c \
	./libs/StdPeriph/src/stm32f10x_flash.c \
	./libs/StdPeriph/src/stm32f10x_dma.c \
	./libs/StdPeriph/src/stm32f10x_tim.c \
	./libs/StdPeriph/src/stm32f10x_iwdg.c \
	./libs/StdPeriph/src/stm32f10x_i2c.c \
	./libs/StdPeriph/src/stm32f10x_cec.c \
	./libs/StdPeriph/src/stm32f10x_usart.c \
	./libs/StdPeriph/src/stm32f10x_dbgmcu.c \
	./libs/StdPeriph/src/stm32f10x_bkp.c \
	./libs/StdPeriph/src/stm32f10x_dac.c \
	./libs/StdPeriph/src/misc.c \
	./libs/StdPeriph/src/stm32f10x_rtc.c \
	./libs/StdPeriph/src/stm32f10x_exti.c \
	./libs/StdPeriph/src/stm32f10x_crc.c \
	./libs/StdPeriph/src/stm32f10x_rcc.c \
	./libs/StdPeriph/src/stm32f10x_spi.c \
	./libs/StdPeriph/src/stm32f10x_sdio.c \
	./libs/StdPeriph/src/stm32f10x_gpio.c \
	./libs/StdPeriph/src/stm32f10x_pwr.c \
	./libs/StdPeriph/src/stm32f10x_adc.c \
	./libs/StdPeriph/src/stm32f10x_fsmc.c

CFLAGS += -I./libs/CMSIS/include -I./libs/StdPeriph/include -I./libs/misc/include

SEGGER_SOURCES := \
	./libs/misc/SEGGER_RTT.c \
	./libs/misc/SEGGER_RTT_printf.c \
