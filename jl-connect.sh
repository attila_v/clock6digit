#!/bin/bash

if [[ $1 != "" ]]; then
    CFGFILE=$1
fi

JLinkExe ${CFGFILE} -device STM32F103C8 -if SWD -speed 4000 -autoconnect 1

