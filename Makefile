PROGRAM := clock6digits

NONE_EABI_PATH ?= /opt/gcc-arm-none-eabi-9-2019-q4-major/bin

SOURCES :=

OBJECTS = $(SOURCES:.c=.o)

MAKE_FLAGS := -g3 -mcpu=cortex-m3 -mthumb

CFLAGS := -O2 -std=gnu11
CFLAGS += -Wall -Wextra -Wpedantic
CFLAGS += $(MAKE_FLAGS) -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections
CFLAGS += -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -DDEBUG
CFLAGS += -I. -I./include
# CFLAGS += -DGREEN_PANEL

-include sources.mk
-include sourcesLibs.mk
-include sourcesFreeRTOS.mk
-include sourcesUsbLib.mk

LDLIBS := -T mem.ld -T libs.ld -T sections.ld -nostartfiles -Xlinker --gc-sections -L./ldscripts -Wl,-Map,$(PROGRAM).map
LDLIBS += $(MAKE_FLAGS)

ARCH := arm
CC := $(NONE_EABI_PATH)/arm-none-eabi-gcc
OBJCOPY := $(NONE_EABI_PATH)/arm-none-eabi-objcopy
SIZE := $(NONE_EABI_PATH)/arm-none-eabi-size

all: $(PROGRAM)

%.o: %.c
	@echo "\tCC $^"
	@$(CC) $(CFLAGS) -c $^ -o $@

$(PROGRAM): $(OBJECTS)
	@$(CC) $^ -o $@.elf $(LDLIBS)
	@$(SIZE) -G $@.elf
	@$(OBJCOPY) -S -O binary $@.elf $@.bin
	@$(OBJCOPY) -O ihex $@.elf $@.hex

pack: $(PROGRAM)
	mkdir pack
	cp $^.bin $^.hex $^.elf jl-connect.sh stm32f103.cfg st-flash_write.sh set-time.py pack/

clean:
	rm -rf $(OBJECTS)

cleanall: clean
	rm *.elf *.hex *.bin *.map

.PHONY: all clean cleanall pack
