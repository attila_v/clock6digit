SOURCES += \
	FreeRTOS/Source/portable/MemMang/heap_1.c \
	FreeRTOS/Source/portable/GCC/ARM_CM3/port.c \
	FreeRTOS/Source/croutine.c \
	FreeRTOS/Source/event_groups.c \
	FreeRTOS/Source/list.c \
	FreeRTOS/Source/queue.c \
	FreeRTOS/Source/tasks.c \
	FreeRTOS/Source/timers.c

CFLAGS += -I./FreeRTOS/Source/include -I./FreeRTOS/Source/portable/GCC/ARM_CM3
