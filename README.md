
# six digit clock with stm32f103c8

view and study the schematic circuit pdf file!

to write the program to MCU (via JLink SWD port), get the 'hex' file from gitlab artifact (or compile it from sources), then:

    ./jl-connect.sh stm32f103.cfg

or with ST-LINK (with 'hex' file too):

    ./st-flash_write.sh



to set the clock, send a message via uart port e.g.:

    #2020,12,4,5,21,9,35,0#

data format is _"year, month, day, weekday, hour, minute, second, calibration"_

    YYYY,MM,DD,wd,hh,mm,ss,cal

(if 'cal' is zero, then the calibration register will not be set)


or use 'set-time.py' python script with '0' offset for stm32 calibration register:

    ./set-time.py

or set time with calibration (e.g. with 11):

    ./set-time.py 11

('set-time.py' works on /dev/ttyUSB0 only!)
