SOURCES += \
			src/main.c \
			src/fifo.c \
			src/uartdebug.c \
			src/ds18b20/ds18b20.c \
			src/ds18b20/ds18b20_hw.c \
			src/rtc/rtc_hw.c \
			src/rtc/rtc.c \
			src/uart/uart.c \
			src/uart/uart_hw.c \
			src/keyboard/keyboard.c \
			src/keyboard/keyboard_hw.c \
			src/display/display_fifo.c \
			src/display/display_hw.c \
			src/display/display.c \

CFLAGS += -I./src
