#!/usr/bin/env python3

from time import sleep
from serial import Serial
from datetime import datetime
import sys

class SetTime(object):
    def __init__(self, port: Serial):
        self.port = port

    def _send(self, data: bytes) -> None:
        self.port.write(data)

if __name__ == '__main__':
    try:
        offset = int(sys.argv[1])
    except (IndexError, ValueError):
        offset = 0

    setTime = SetTime(Serial(port='/dev/ttyUSB0', baudrate=115200, timeout=30))
    sleep(2)    # initialization
    t = datetime.now().strftime('#%Y,%m,%d,1,%H,%M,%S,' + str(offset) + '#')
    print(t);

    try:
        setTime._send(t.encode("utf-8"))
    except (KeyboardInterrupt, BrokenPipeError):
        exit(1)
