#ifndef UARTDEBUG_H
#define UARTDEBUG_H

#include <stdint.h>

void debug(const char* str);
void debugCR(const char* str);
void debugNum(uint32_t num, int lead);
void debugNumCR(uint32_t num, int lead);
void uartDebug(const char* str, int len);

#endif // UARTDEBUG_H
