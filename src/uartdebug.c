#include "uartdebug.h"
#include "uart/uart.h"
#include <string.h>

void uartDebug(const char* str, int len) {
	int n = 0;
	while (n < len) {
		uart1Put(*(str + n) & 0xFF);
		++n;
	}
}

void debugNum(uint32_t num, int lead) {
	int tens = 1000000000;
	while (num) {
		if (num / tens) break;
		tens /= 10;
		if (lead) {
			uart1Put('0');
		}
	}
	if (!num && !lead) {
		uart1Put('0');
	} else {
		while (num || tens) {
			uart1Put((num / tens + '0') & 0xff);
			num %= tens;
			tens /= 10;
		}
	}
}

void debugNumCR(uint32_t num, int lead) {
	debugNum(num, lead);
	uart1Put('\r');
	uart1Put('\n');
}

void debug(const char* str) {
	uartDebug(str, strlen(str));
}

void debugCR(const char* str) {
	debug(str);
	uart1Put('\r');
	uart1Put('\n');
}
