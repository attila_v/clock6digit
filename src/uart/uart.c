// https://embedds.com/programming-stm32-usart-using-gcc-tools-part-1/

#include "uart.h"
#include "rtc/rtc.h"
#include "stm32f10x_usart.h"

#include <string.h>

#define RECV_BUFFER_SIZE 32

static char recvByte;
static char recvStr[RECV_BUFFER_SIZE];
static int recvBufferPosition = 0;
static int validData = 0;
static UARTSTAT uartStat;

char getUartByte(void) { return recvByte; }
UARTSTAT getUartStat(void) { return uartStat; }

static void evalReceivedByte(void)
{
	if (!recvBufferPosition && (recvByte == '#')) {
		validData = 1;
		return;
	}
	if (validData) {
		if (recvByte == '#') {
			if (recvBufferPosition < RECV_BUFFER_SIZE) {
				// send data
				sendDataTotRtc(&recvStr[0], recvBufferPosition);
			}
			validData = 0;
			recvBufferPosition = 0;
			memset(&recvStr[0], 0, sizeof(recvStr));
		} else {
			recvStr[recvBufferPosition] = recvByte;
			++recvBufferPosition;
		}
	}
}

void getData(void)
{
	recvByte = (char)USART_ReceiveData(USART1);
	uartStat = UART_RECEIVED;
}

/**********************************************************
 * USART1 interrupt request handler:
 *********************************************************/
void USART1_IRQHandler(void)
{
	/* RXNE handler */
	if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) {
		getData();
//		uart1Put(recvByte);  // echo back the received char, it corrupts eval function
		evalReceivedByte();
	}
}

void uart1Put(uint8_t ch)
{
	USART_SendData(USART1, (uint8_t) ch);
	//Loop until the end of transmission
	while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET) {;}
	uartStat = UART_SENT;
}

char uart1Get(void)
{
	if (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) != RESET) {
		getData();
	}
	uartStat = UART_NOP;
	return recvByte;
}
