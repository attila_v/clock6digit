#ifndef UART_H
#define UART_H

#include <stdint.h>

typedef enum {
	UART_NOP,
	UART_SENT,
	UART_RECEIVED
} UARTSTAT;

char getUartByte(void);
UARTSTAT getUartStat(void);


void uart1Put(uint8_t ch);
char uart1Get(void);

#endif // UART_H
