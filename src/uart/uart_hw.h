/*
 * uart_hw.h
 *
 *  Created on: Mar 28, 2020
 *      Author: attila
 */

#ifndef UART_HW_H
#define UART_HW_H

#include "stm32f10x.h"

void uart1Init(void);

#endif /* UART_HW_H */
