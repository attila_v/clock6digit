/*
 * ds18b20_hw.h
 *
 *  Created on: Nov 1, 2020
 *      Author: attila
 */

#ifndef DS18B20_HW_H
#define DS18B20_HW_H

#include <stdint.h>

uint8_t reset(void);
uint8_t readBit(void);
void writeByte(uint8_t byte);
uint8_t readByte(void);

void ds18b20GpioSet(void);

#endif /* DS18B20_HW_H */
