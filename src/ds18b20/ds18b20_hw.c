/*
 * ds18b20_hw.c
 *
 *  Created on: Nov 1, 2020
 *      Author: attila
 *
 *  // Project/STM32F10x_StdPeriph_Examples/TIM/TIM1_Synchro/main.c
 */

#include "ds18b20_hw.h"
#include "FreeRTOS.h"
#include "task.h"

#include <string.h>
#include <stdio.h>

static GPIO_InitTypeDef GPIO_InitStructure;

#define DS_PORT GPIOB
#define DS_PIN  GPIO_Pin_15

static __IO int8_t tempDigit;
static __IO uint16_t tempDecimal;

#pragma GCC push_options
#pragma GCC optimize ("O0")
void delayUs(uint32_t us)
{
	volatile uint32_t counter = 7 * us; // at 72MHz
	while (counter--);
}
#pragma GCC pop_options

static void setIoToOutput(void)
{
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
	GPIO_Init(DS_PORT, &GPIO_InitStructure);
}

static void setIoToInput(void)
{
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(DS_PORT, &GPIO_InitStructure);
}

static void writeBit(uint8_t bit)
{
	setIoToOutput();
	GPIO_ResetBits(DS_PORT, DS_PIN);
//	GPIOB->ODR &= ~DS_PIN;
	delayUs(1);
	if (bit) {
		setIoToInput();
	}
	delayUs(60);
	setIoToInput();
}

uint8_t reset(void)
{
	uint8_t ret;
	taskENTER_CRITICAL();
	setIoToOutput();
	GPIO_ResetBits(DS_PORT, DS_PIN);
//	GPIOB->ODR &= ~DS_PIN;
	delayUs(480);
	setIoToInput();
	delayUs(60);
	ret = GPIO_ReadInputDataBit(DS_PORT, DS_PIN);
	delayUs(420);
	taskEXIT_CRITICAL();
	return ret; // 0=OK, 1=Error
}

uint8_t readBit(void)
{
	uint8_t ret;
	setIoToOutput();
	GPIO_ResetBits(DS_PORT, DS_PIN);
//	GPIOB->ODR &= ~DS_PIN;
	delayUs(1);
	setIoToInput();
	delayUs(14);
	ret = GPIO_ReadInputDataBit(DS_PORT, DS_PIN);
//	ret = GPIO_ReadInputData(DS_PORT) & DS_PIN;
//	ret = GPIOB->IDR & DS_PIN;
	delayUs(45);
	return ret ? 1 : 0;
}

void writeByte(uint8_t byte)
{
	int x = 8;
	taskENTER_CRITICAL();
	while (x) {
		delayUs(1);
		writeBit(byte & 1);
		byte >>= 1;
		--x;
	}
	taskEXIT_CRITICAL();
}

uint8_t readByte(void)
{
	int x = 8;
	uint8_t ret = 0;
	taskENTER_CRITICAL();
	while (x) {
		ret >>= 1;
		ret |= (readBit() << 7);
		--x;
	}
	taskEXIT_CRITICAL();
	return ret;
}

void ds18b20GpioSet(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_InitStructure.GPIO_Pin = DS_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(DS_PORT, &GPIO_InitStructure);
	GPIO_SetBits(DS_PORT, DS_PIN);
}
