/*
 * ds18b20.h
 *
 *  Created on: May 7, 2019
 *      Author: attila
 */

#ifndef DS18B20_DS18B20_H_
#define DS18B20_DS18B20_H_

#include "stm32f10x.h"

void prvDs18b20Task(void *pvParameters);

int8_t getTempDigit(void);
uint16_t getTempDecimal(void);

#endif /* DS18B20_DS18B20_H_ */
