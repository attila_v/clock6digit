/*
 * ds18b20.c
 *
 *  Created on: May 7, 2019
 *      Author: attila
 *
 *  // Project/STM32F10x_StdPeriph_Examples/TIM/TIM1_Synchro/main.c
 */

#include "ds18b20.h"
#include "ds18b20_hw.h"
#include "display/display_fifo.h"
#include "uartdebug.h"
#include "FreeRTOS.h"
#include "task.h"

#define CONVERTTEMP 0x44
#define READSCRATCHPAD 0xbe
#define SKIPROM 0xcc

static __IO int8_t tempDigit;
static __IO uint16_t tempDecimal;
static __IO uint8_t minus;

static void getTemp(void)
{
	uint8_t t1, t2;
	if (!reset()) {
		writeByte(SKIPROM);
		writeByte(CONVERTTEMP);
		while (!readBit());
	}

	if (!reset()) {
		writeByte(SKIPROM);
		writeByte(READSCRATCHPAD);
	}
	t1 = readByte();
	t2 = readByte();

	minus = (!reset()) && (t2 & 0xf0) ? 1 : 0;

	tempDecimal = minus ? (((~(t1 & 0x0f)) & 0xff) + 1) & 0x0f : t1 & 0x0f;
	tempDecimal *= 625; // 0.0625

	tempDigit = t1 >> 4;
	tempDigit |= (t2 & 0x07) << 4;

	if (minus) {
		tempDigit = (~tempDigit) & 0x7f;
		if (!tempDecimal) {
			++tempDigit;
		}
	}
}

static void tempDebug()
{
	char tempStr[6];
	int16_t temp = tempDigit;
	tempStr[0] = minus ? '-' : '+';
	if (temp > 99) temp = 99;
	if (temp < 10) {
		tempStr[1] = '0';
	} else {
		tempStr[1] = (temp / 10) + '0';
	}
	tempStr[2] = (temp % 10) + '0';
	tempStr[3] = '.';
	tempStr[4] = (tempDecimal / 1000) + '0';
	tempStr[5] = 0;

	debugCR(tempStr);
}

static void ds18b20Main(void)
{
	static char buffer[4];
	while (1) {
		getTemp();
		buffer[0] = 2;
		buffer[1] = minus ? 1 : 0;
		buffer[2] = (tempDigit > 99) ? 99 : tempDigit;
		buffer[3] = tempDecimal / 1000;
		writeDisplayFifo(buffer, 4);
	
		tempDebug();
		vTaskDelay(3000);
	}
}

void prvDs18b20Task(void *pvParameters)
{
	if (pvParameters) {
		return;
	}
	ds18b20GpioSet();
	ds18b20Main();
}
