#include "fifo.h"

#include <stdbool.h>

static bool semaphoreTake(xSemaphoreHandle semaphore)
{
	if (semaphore) {
		xSemaphoreTake(semaphore, portMAX_DELAY);
		return true;
	}
	return false;
}

int getFifoContentSize(RingBuffer* rb)
{
	return (rb->wp + rb->fifoSize - rb->rp) & (rb->fifoSize - 1);
}

void writeFifo(RingBuffer* rb, const char* sourceBuffer, int sourceBufferLen)
{
	if (semaphoreTake(rb->semaphore)) {
		if (sourceBufferLen <= (rb->fifoSize - getFifoContentSize(rb))) {
			while (sourceBufferLen) {
				rb->fifo[rb->wp] = *sourceBuffer;
				rb->wp = (rb->wp + 1) & (rb->fifoSize - 1);
				++sourceBuffer;
				--sourceBufferLen;
			}
		}
		xSemaphoreGive(rb->semaphore);
	}
}

int readFifo(RingBuffer* rb, char* targetBuffer)
{
	int ret = 0;
	if (semaphoreTake(rb->semaphore)) {
		while (getFifoContentSize(rb)) {
			*targetBuffer = rb->fifo[rb->rp];
			rb->rp = (rb->rp + 1) & (rb->fifoSize - 1);
			++targetBuffer;
			++ret;
		}
		xSemaphoreGive(rb->semaphore);
	}
	return ret;
}
