/*
 * rtc.h
 *
 *  Created on: Sep 25, 2019
 *      Author: attila
 */

#ifndef RTC_H_
#define RTC_H_

#include "rtc_hw.h"
#include "stm32f10x.h"

void sendDataTotRtc(const char* data, int len);
void prvRtcTask(void *pvParameters);

#endif /* RTC_H_ */
