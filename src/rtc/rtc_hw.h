#ifndef RTC_HW_H
#define RTC_HW_H

#include <stdint.h>

typedef struct
{
	uint8_t rtcHours;
	uint8_t rtcMinutes;
	uint8_t rtcSeconds;
	uint8_t rtcDate;
	uint8_t rtcWday;
	uint8_t rtcMonth;
	uint16_t rtcYear;
} RTC_DateTimeTypeDef;

unsigned char RTC_Init(void);

void RTC_GetDateTime(uint32_t RTC_Counter, RTC_DateTimeTypeDef* RTC_DateTimeStruct);

uint32_t RTC_GetRTC_Counter(RTC_DateTimeTypeDef* RTC_DateTimeStruct);

#endif // RTC_HW_H
