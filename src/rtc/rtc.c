/*
 * rtc.c
 *
 *  Created on: Sep 25, 2019
 *      Author: attila
 *
 */

#include "rtc.h"
#include "display/display_fifo.h"
#include "uartdebug.h"

#include "FreeRTOS.h"
#include "task.h"

#include <string.h>

static void setCalibration(uint16_t calib)
{
	BKP_WriteBackupRegister(BKP_DR1, calib);
	BKP_SetRTCCalibrationValue(BKP_ReadBackupRegister(BKP_DR1));
}

static void setRtc(RTC_DateTimeTypeDef* rtcDateTime)
{
	RTC_SetCounter(RTC_GetRTC_Counter(rtcDateTime));
}

static void sendToDisplay(RTC_DateTimeTypeDef* rtcDateTime)
{
	static char buffer[4];
	buffer[0] = 0;
	buffer[1] = rtcDateTime->rtcHours;
	buffer[2] = rtcDateTime->rtcMinutes;
	buffer[3] = rtcDateTime->rtcSeconds;
	writeDisplayFifo(buffer, 4);
	buffer[0] = 1;
	buffer[1] = rtcDateTime->rtcMonth;
	buffer[2] = rtcDateTime->rtcDate;
	buffer[3] = rtcDateTime->rtcWday + 1; // 0 == monday
	writeDisplayFifo(buffer, 4);
}

static void rtcMain(void)
{
	if (RTC_Init()) {
//											hh  mm  ss  dd  w  MM  year
		RTC_DateTimeTypeDef rtcDateTime = { 20, 59, 10, 05, 4, 11, 2020 };
		vTaskDelay(500);
		setRtc(&rtcDateTime);
		BKP_WriteBackupRegister(BKP_DR1, 0x0e);
	}
	BKP_SetRTCCalibrationValue(BKP_ReadBackupRegister(BKP_DR1));

	while (1) {
		uint32_t rtcCounter = RTC_GetCounter();
		RTC_DateTimeTypeDef rtcDateTime;
		RTC_GetDateTime(rtcCounter, &rtcDateTime);

		sendToDisplay(&rtcDateTime);

		while (rtcCounter == RTC_GetCounter()) {
			vTaskDelay(100);
		}

	}
}
static void setRtcDateTime(const uint16_t* buffer)
{
	if (!buffer) return;
	RTC_DateTimeTypeDef rtcDateTime;
	rtcDateTime.rtcYear = buffer[0];
	rtcDateTime.rtcMonth = buffer[1];
	rtcDateTime.rtcDate = buffer[2];
	rtcDateTime.rtcWday = buffer[3];
	rtcDateTime.rtcHours = buffer[4];
	rtcDateTime.rtcMinutes = buffer[5];
	rtcDateTime.rtcSeconds = buffer[6];
	setRtc(&rtcDateTime);
}
/*-----------------------------------------------------------*/
void sendDataTotRtc(const char* data, int len)
{
	if (!data) return;
	debugCR(&data[0]);
	debugNumCR(len, 0);

	int pos = 0;
	int buffPos = 0;
	uint16_t buff[8];
	memset(&buff[0], 0, sizeof(buff));
	while ((len > pos) && (buffPos < 8)) {
		uint16_t value = 0;
		while ((data[pos] != ',') && (len > pos)) {
			if (data[pos] <= '9' && data[pos] >= '0') {
				value = value * 10 + (data[pos] - '0');
			}
			++pos;
		}
		buff[buffPos] = value;
		++buffPos;
		++pos;
	}
	if (buffPos < 7) return;
	setRtcDateTime(&buff[0]);
	if (buff[7]) {
		setCalibration(buff[7]);
	}
}

void prvRtcTask(void *pvParameters)
{
	if (pvParameters) {
		return;
	}
	rtcMain();
}
