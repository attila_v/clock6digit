// https://github.com/avislab/STM32F103/blob/master/Example_RTC/main.c

#include "rtc_hw.h"
#include "stm32f10x_rtc.h"
#include "stm32f10x_rcc.h"

unsigned char RTC_Init(void) {
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
	PWR_BackupAccessCmd(ENABLE);
	if ((RCC->BDCR & RCC_BDCR_RTCEN) != RCC_BDCR_RTCEN)
	{
		RCC_BackupResetCmd(ENABLE);
		RCC_BackupResetCmd(DISABLE);

		RCC_LSEConfig(RCC_LSE_ON);
		while ((RCC->BDCR & RCC_BDCR_LSERDY) != RCC_BDCR_LSERDY) {}
		RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

		RTC_SetPrescaler(0x7FFF);

		RCC_RTCCLKCmd(ENABLE);

		RTC_WaitForSynchro();

		return 1;
	}
	return 0;
}

// (UnixTime = 00:00:00 01.01.1970 = JD0 = 2440588)
#define JULIAN_DATE_BASE	2440588

// Get current date
void RTC_GetDateTime(uint32_t RTC_Counter, RTC_DateTimeTypeDef* RTC_DateTimeStruct) {
	unsigned long a, b, c, d, e, m;
	uint64_t jd = 0;
	uint64_t jdn = 0;

	jd = ((RTC_Counter + 43200) / (86400 >> 1)) + (2440587 << 1) + 1;
	jdn = jd >> 1;

	RTC_DateTimeStruct->rtcSeconds = RTC_Counter % 60;

	RTC_DateTimeStruct->rtcMinutes = (RTC_Counter / 60) % 60;

	RTC_DateTimeStruct->rtcHours = (RTC_Counter / 3600) % 24;

	RTC_DateTimeStruct->rtcWday = jdn % 7;

	a = jdn + 32044;
	b = (4*a+3)/146097;
	c = a - (146097*b)/4;
	d = (4*c+3)/1461;
	e = c - (1461*d)/4;
	m = (5*e+2)/153;
	RTC_DateTimeStruct->rtcDate = e - (153*m+2)/5 + 1;
	RTC_DateTimeStruct->rtcMonth = m + 3 - 12*(m/10);
	RTC_DateTimeStruct->rtcYear = 100*b + d - 4800 + (m/10);
}

// Convert Date to Counter
uint32_t RTC_GetRTC_Counter(RTC_DateTimeTypeDef* RTC_DateTimeStruct) {
	uint8_t a;
	uint16_t y;
	uint8_t m;
	uint32_t JDN;

	a=(14-RTC_DateTimeStruct->rtcMonth)/12;
	y=RTC_DateTimeStruct->rtcYear+4800-a;
	m=RTC_DateTimeStruct->rtcMonth+(12*a)-3;

	JDN=RTC_DateTimeStruct->rtcDate;
	JDN+=(153*m+2)/5;
	JDN+=365*y;
	JDN+=y/4;
	JDN+=-y/100;
	JDN+=y/400;
	JDN = JDN -32045;
	JDN = JDN - JULIAN_DATE_BASE;
	JDN*=86400;
	JDN+=(RTC_DateTimeStruct->rtcHours*3600);
	JDN+=(RTC_DateTimeStruct->rtcMinutes*60);
	JDN+=(RTC_DateTimeStruct->rtcSeconds);

	return JDN;
}


