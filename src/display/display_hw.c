#include "display_hw.h"
#include "FreeRTOS.h"
#include "task.h"

#define SEGMENT_PORT GPIOA
#define DIGIT_PORT GPIOB
#define DIGIT_DELAY (2 / portTICK_PERIOD_MS)

#define MAX_DIGITS 6

static const int GPIO_PINS[MAX_DIGITS] = { GPIO_Pin_0, GPIO_Pin_1, GPIO_Pin_2, GPIO_Pin_3, GPIO_Pin_4, GPIO_Pin_5 };

//                                        0     1     2     3     4     5     6     7     8     9     a     b     c     d     e     f     dz    -
static const char DIGITS[] =          { 0x40, 0x79, 0x24, 0x30, 0x19, 0x12, 0x02, 0x78, 0x00, 0x10, 0x08, 0x03, 0x46, 0x21, 0x06, 0x0e, 0x7f, 0x3f };
static const char BOTTOMUP_DIGITS[] = { 0x40, 0x4f, 0x24, 0x06, 0x0b, 0x12, 0x10, 0x47, 0x00, 0x02, 0x01, 0x18, 0x70, 0x0c, 0x30, 0x31, 0x7f, 0x3f };
static int noDisplayZero = 0; // &1 > 1st; &2 > 2nd; &4 > 3rd &8 > 4th; &16 > 5th; &32 > 6th digit
static int bottomUp = 0; // bottom up == 1

void setDz(int dz) { noDisplayZero = dz; }

static void setSegments(size_t segment, char ndz, const char* digits)
{
	if (segment > sizeof(DIGITS)) {
		 return;
	}
	if (ndz && !segment) {
		segment = 0x10;
	}
	GPIO_WriteBit(SEGMENT_PORT, GPIO_Pin_0, digits[segment] & 0x01 ? Bit_SET : Bit_RESET);
	GPIO_WriteBit(SEGMENT_PORT, GPIO_Pin_1, digits[segment] & 0x02 ? Bit_SET : Bit_RESET);
	GPIO_WriteBit(SEGMENT_PORT, GPIO_Pin_2, digits[segment] & 0x04 ? Bit_SET : Bit_RESET);
	GPIO_WriteBit(SEGMENT_PORT, GPIO_Pin_3, digits[segment] & 0x08 ? Bit_SET : Bit_RESET);
	GPIO_WriteBit(SEGMENT_PORT, GPIO_Pin_4, digits[segment] & 0x10 ? Bit_SET : Bit_RESET);
	GPIO_WriteBit(SEGMENT_PORT, GPIO_Pin_5, digits[segment] & 0x20 ? Bit_SET : Bit_RESET);
	GPIO_WriteBit(SEGMENT_PORT, GPIO_Pin_6, digits[segment] & 0x40 ? Bit_SET : Bit_RESET);
}

static void showDigits(const char* digit, const char* overDigit, int sign)
{
	int i;
	const char* digits = bottomUp ? &BOTTOMUP_DIGITS[0] : &DIGITS[0];

	for (i = 0; i < MAX_DIGITS; ++i) {
		GPIO_SetBits(DIGIT_PORT, GPIO_PINS[i]);
		if (sign & (1 << i)) {
			setSegments(overDigit[i] + 0x10, 0, digits);
		} else {
			setSegments(digit[i], noDisplayZero & (1 << i), digits);
		}
		GPIO_ResetBits(DIGIT_PORT, GPIO_PINS[i]);
		vTaskDelay(DIGIT_DELAY);
		GPIO_SetBits(DIGIT_PORT, GPIO_PINS[i]);
	}
}

static void displayBottomUp(char d1, char d2, char d3, int scale, int sign)
{
	const char digit[MAX_DIGITS] = { d3 % scale, d3 / scale, d2 % scale, d2 / scale, d1 % scale, d1 / scale };
	const char overDigit[MAX_DIGITS] = { d3, d3, d2, d2, d1, d1 };
	showDigits(&digit[0], &overDigit[0], sign);
}

static void displayStandUp(char d1, char d2, char d3, int scale, int sign)
{
	const char digit[MAX_DIGITS] = { d1 / scale, d1 % scale, d2 / scale, d2 % scale, d3 / scale, d3 % scale };
	const char overDigit[MAX_DIGITS] = { d1, d1, d2, d2, d3, d3 };
	showDigits(&digit[0], &overDigit[0], sign);
}

void displayDigits(const char* message, int scale)
{
	char d1 = message[0];
	char d2 = message[1];
	char d3 = message[2];
	int sign = message[3];
	bottomUp = scale & 0x80 ? 1 : 0;
	scale &= 0x7f;
	if (scale == 10) {
		if (d1 > 99) d1 = 99;
		if (d2 > 99) d2 = 99;
		if (d3 > 99) d3 = 99;
	} else if (scale == 8) {
		if (d1 > 63) d1 = 63;
		if (d2 > 63) d2 = 63;
		if (d3 > 63) d3 = 63;
	}

	if (bottomUp) {
		displayBottomUp(d1, d2, d3, scale, sign);
	} else {
		displayStandUp(d1, d2, d3, scale, sign);
	}
}

void digitsGpioSet(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(SEGMENT_PORT, &GPIO_InitStructure);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
// https://community.st.com/s/question/0D50X00009XkZMmSAN/stm32f103-pb3-just-doesnt-work-as-gpio
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(DIGIT_PORT, &GPIO_InitStructure);
	GPIO_SetBits(DIGIT_PORT, GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5);
}
