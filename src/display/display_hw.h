/*
 *  display_hw.h
 *
 *  Created on: Oct 31, 2020
 *      Author: attila
 */


#ifndef DISPLAY_HW_H
#define DISPLAY_HW_H

void setDz(int ndz);
void displayDigits(const char* message, int scale);
void digitsGpioSet(void);

#endif // DISPLAY_HW_H
