#include "display.h"
#include "display_hw.h"
#include "display_fifo.h"
#include "uartdebug.h"
#include "FreeRTOS.h"
#include "task.h"

#define MSG_TYPES 8 // 0: clock, 1: date, 2: temperature, 3: test, 8: set mode (types, nlz, oct/dec/hex)

static char messages[MSG_TYPES][4]; // digit 1-2, digit 3-4, digit 4-5, sign
static char actualMode[3] = { 0, 0, 10 }; // type, dz, scale
static char buffer[DISPLAYFIFOSIZE];

static void debugFifo(const char* buff)
{
	return;
	char d[12];
	d[0] = buff[0] / 10 + '0';
	d[1] = buff[0] % 10 + '0';
	d[2] = ' ';
	d[3] = buff[1] / 10 + '0';
	d[4] = buff[1] % 10 + '0';
	d[5] = ' ';
	d[6] = buff[2] / 10 + '0';
	d[7] = buff[2] % 10 + '0';
	d[8] = ' ';
	d[9] = buff[3] / 10 + '0';
	d[10] = buff[3] % 10 + '0';
	d[11] = 0;
	debugCR(d);
}

static void evalFifo(const char* buff)
{
	debugFifo(buff);
	if ((buff[0] & MSG_TYPES) == MSG_TYPES) {
		actualMode[0] = buff[1];
		if (actualMode[1] != buff[2]) {
			actualMode[1] = buff[2];
			setDz(actualMode[1]);
		}
		actualMode[2] = buff[3];
		return;
	}
	const int m = buff[0] & (MSG_TYPES - 1);

	messages[m][0] = buff[1];
	messages[m][1] = buff[2];
	messages[m][2] = buff[3];
	messages[m][3] = 0x00;

	if ((m == 2) && (buff[1])) {
		// set temperature minus sign
		messages[m][3] = 0x02;
	}
}

static void readFifo(void)
{
	int readedSize = readDisplayFifo(buffer);
	if (!readedSize) return;
	if (readedSize % 4) {
		actualMode[0] = 3;
		return;
	}
	while (readedSize > 0) {
		evalFifo(buffer + readedSize - 4);
		readedSize -= 4;
	}
}

static void digitsMain()
{
	messages[3][0] = 12;
	messages[3][1] = 34;
	messages[3][2] = 56;
	while (1) {
		readFifo();
		const int m = actualMode[0];
		displayDigits(&messages[m][0], actualMode[2]);
	}
}

void prvDisplayTask(void *pvParameters)
{
	if (pvParameters) {
		return;
	}
	displayFifoInit();
	digitsGpioSet();
	digitsMain();
}
