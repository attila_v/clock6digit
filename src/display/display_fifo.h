#ifndef DISPLAY_FIFO_H
#define DISPLAY_FIFO_H

#define DISPLAYFIFOSIZE (1 << 8) // 1 << 8 = 256

void writeDisplayFifo(const char* sourceBuffer, int sourceBufferLen);

int readDisplayFifo(char* targetBuffer);

void displayFifoInit(void);

#endif // DISPLAY_FIFO_H
