#include "display_fifo.h"
#include "fifo.h"

static char fifo[DISPLAYFIFOSIZE];

static RingBuffer displayFifo = { fifo, DISPLAYFIFOSIZE, 0, 0, 0 };

void writeDisplayFifo(const char* sourceBuffer, int sourceBufferLen)
{
	writeFifo(&displayFifo, sourceBuffer, sourceBufferLen);
}

int readDisplayFifo(char* targetBuffer)
{
	return readFifo(&displayFifo, targetBuffer);
}

void displayFifoInit(void)
{
	displayFifo.semaphore = xSemaphoreCreateBinary();
	xSemaphoreGive(displayFifo.semaphore);
}
