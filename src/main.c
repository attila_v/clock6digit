/*
 * FreeRTOS Coding Standard and Style Guide:
 * http://www.freertos.org/FreeRTOS-Coding-Standard-and-Style-Guide.html
 */

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"

/* STM32 Library includes. */
#include "stm32f10x.h"

// #include "libs/misc/SEGGER_RTT.h"
#include "ds18b20/ds18b20.h"
#include "rtc/rtc.h"
#include "uart/uart_hw.h"
#include "keyboard/keyboard.h"
#include "display/display.h"

#include <stdio.h>
#include <string.h>

/* Priorities at which the tasks are created. */
#define mainDS18B20_TASK_PRIORITY			(tskIDLE_PRIORITY)
#define mainRTC_TASK_PRIORITY				(tskIDLE_PRIORITY)
#define mainKEYBOARD_TASK_PRIORITY			(tskIDLE_PRIORITY)
#define mainDISPLAY_TASK_PRIORITY			(tskIDLE_PRIORITY)

/* ----- Private method definitions ---------------------------------------- */
static void prvSetupHardware(void);

__IO uint32_t usTicks;
void Systick_Handler(void)
{
	if (usTicks) --usTicks;
}

void pwmGpioSet(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 | RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE);
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_ResetBits(GPIOA, GPIO_Pin_8);
}

/* ----- Main -------------------------------------------------------------- */
int main()
{
	/* Configure the hardware */
	prvSetupHardware();

//	setvbuf(stdout, 0, _IONBF, 0);

//	SEGGER_RTT_WriteString(0, "six digits clock is initializing\n");

	pwmGpioSet();

	/* Init uart */
	uart1Init();

	/* Create display task */
	xTaskCreate(prvDisplayTask, "display", configMINIMAL_STACK_SIZE, 0, mainDISPLAY_TASK_PRIORITY, 0);

	/* Create keyboard task */
	xTaskCreate(prvKeyboardTask, "Keyboard", configMINIMAL_STACK_SIZE, 0, mainKEYBOARD_TASK_PRIORITY, 0);

	/* Create ds18b20 task */
	xTaskCreate(prvDs18b20Task, "Temp", configMINIMAL_STACK_SIZE, 0, mainDS18B20_TASK_PRIORITY, 0);

	/* Create rtc task */
	xTaskCreate(prvRtcTask, "Rtc", configMINIMAL_STACK_SIZE, 0, mainRTC_TASK_PRIORITY, 0);

	/* Start the scheduler */
	vTaskStartScheduler();

	/* If all is well, the scheduler will now be running, and the following line
	will never be reached.  If the following line does execute, then there was
	insufficient FreeRTOS heap memory available for the idle and/or timer tasks
	to be created.  See the memory management section on the FreeRTOS web site
	for more details. */
	while (1);
}
/*-----------------------------------------------------------*/

static void prvSetupHardware(void)
{
	/* Ensure that all 4 interrupt priority bits are used as the pre-emption priority. */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
}
/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook(void)
{
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues, software
	timers, and semaphores.  The size of the FreeRTOS heap is set by the
	configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */
	while (1);
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook(TaskHandle_t pxTask, char *pcTaskName)
{
	(void) pcTaskName;
	(void) pxTask;

	/* Run time stack overflow checking is performed if
	configconfigCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
	function is called if a stack overflow is detected. */
	while (1);
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook(void)
{
	volatile size_t xFreeStackSpace;

	/* This function is called on each cycle of the idle task.  In this case it
	does nothing useful, other than report the amount of FreeRTOS heap that
	remains unallocated. */
	xFreeStackSpace = xPortGetFreeHeapSize();

	if (xFreeStackSpace > 100)
	{
		/* By now, the kernel has allocated everything it is going to, so
		if there is a lot of heap remaining unallocated then
		the value of configTOTAL_HEAP_SIZE in FreeRTOSConfig.h can be
		reduced accordingly. */
	}
}

#ifdef DEBUG
/* The fault handler implementation calls a function called
prvGetRegistersFromStack(). */
void HardFault_Handler(void)
{
    __asm volatile
    (
        " tst lr, #4                                                \n"
        " ite eq                                                    \n"
        " mrseq r0, msp                                             \n"
        " mrsne r0, psp                                             \n"
        " ldr r1, [r0, #24]                                         \n"
        " ldr r2, handler2_address_const                            \n"
        " bx r2                                                     \n"
        " handler2_address_const: .word prvGetRegistersFromStack    \n"
    );
}

void prvGetRegistersFromStack(uint32_t *pulFaultStackAddress)
{
	/* These are volatile to try and prevent the compiler/linker optimising them
	away as the variables never actually get used.  If the debugger won't show the
	values of the variables, make them global my moving their declaration outside
	of this function. */
	volatile uint32_t r0;
	volatile uint32_t r1;
	volatile uint32_t r2;
	volatile uint32_t r3;
	volatile uint32_t r12;
	volatile uint32_t lr; /* Link register. */
	volatile uint32_t pc; /* Program counter. */
	volatile uint32_t psr;/* Program status register. */

    r0 = pulFaultStackAddress[0];
    r1 = pulFaultStackAddress[1];
    r2 = pulFaultStackAddress[2];
    r3 = pulFaultStackAddress[3];

    r12 = pulFaultStackAddress[4];
    lr = pulFaultStackAddress[5];
    pc = pulFaultStackAddress[6];
    psr = pulFaultStackAddress[7];

    /* When the following line is hit, the variables contain the register values. */
    while (1);

    /* These lines help prevent getting warnings from compiler about unused variables */
    r0 = r1 = r2 = r3 = r12 = lr = pc = psr = 0;
    r0++;
}

#endif // #ifdef DEBUG
