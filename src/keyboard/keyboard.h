/*
 * keyboard.h
 *
 *  Created on: Jun 03, 2020
 *      Author: attila
 */

#ifndef KEYBOARD_H_
#define KEYBOARD_H_

char getKey(void);
char getLongkey(void);

void prvKeyboardTask(void *pvParameters);

#endif /* KEYBOARD_H_ */
