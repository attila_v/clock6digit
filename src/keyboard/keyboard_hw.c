/*
 * keyboard_hw.c
 *
 *  Created on: Nov 01, 2020
 *      Author: attila
 *
 */

#include "keyboard.h"
#include "FreeRTOS.h"
#include "task.h"

#define	KB_PORT	GPIOB

#define	KEY1	GPIO_Pin_8
#define	KEY2	GPIO_Pin_9
#define	KEY3	GPIO_Pin_10

char decodeKey(void)
{
	uint16_t portin = GPIO_ReadInputData(KB_PORT);
	if (!(portin & KEY3)) return 3;
	if (!(portin & KEY2)) return 2;
	if (!(portin & KEY1)) return 1;
	return 0;
}

void keyboardGpioSet(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_InitStructure.GPIO_Pin = KEY1 | KEY2 | KEY3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(KB_PORT, &GPIO_InitStructure);
	GPIO_SetBits(KB_PORT, KEY1 | KEY2 | KEY3);
}
