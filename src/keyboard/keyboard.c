/*
 * keyboard.c
 *
 *  Created on: Jun 03, 2020
 *      Author: attila
 *
 */

#include "keyboard.h"
#include "keyboard_hw.h"
#include "display/display_fifo.h"
#include "FreeRTOS.h"
#include "task.h"
#include "uartdebug.h"
#include "stm32f10x.h"

#define KEY1STATES 6

static char key;
static char longkey;

static size_t k1s;

static const int key1states[KEY1STATES] = { 10, 0x80 + 10, 8, 0x80 + 8, 16, 0x80 + 16 };

static void printDebug(void)
{
	static char buf[4];
	buf[0] = key + '0';
	buf[1] = ' ';
	buf[2] = longkey + '0';
	buf[3] = 0;
	debugCR(buf);
}

static void sendMsg(char d1, char d2, char d3)
{
	char buffer[4];
	buffer[0] = 8;
	buffer[1] = d1;
	buffer[2] = d2;
	buffer[3] = d3;
	writeDisplayFifo(buffer, 4);
}

static void keyboardMain(void)
{
	static char shortkey;
	int counter = 0;
	while (1) {
		key = decodeKey();
		if (key > 0) {
			if (shortkey != key) {
				shortkey = key;
				counter = 0;
			}
			if (counter == 5) {
				longkey = shortkey;
				counter = 0;
			} else {
				++counter;
			}
			if (longkey == 1) {
				longkey = 0;
				++k1s;
				if (k1s >= KEY1STATES) k1s = 0;
				sendMsg(0, 0, key1states[k1s]); // hhmmss, all zero, step state
			}
			else if (shortkey == 1) sendMsg(0, 0, key1states[k1s]); // hhmmss, all zero, state
			else if (shortkey == 2) sendMsg(1, (k1s&1)?2:16, key1states[k1s]); // MMDDdd, weekday no leading zero, state
			else if (shortkey == 3) sendMsg(2, (k1s&1)?58:23, key1states[k1s]); // temperature, no zeros & decimal no leading zero, state

			BKP_WriteBackupRegister(BKP_DR2, k1s & 0xff);
			printDebug();
		} else {
			longkey = shortkey = 0;
		}
		vTaskDelay(200 / portTICK_PERIOD_MS);
	}
}

/*-----------------------------------------------------------*/
char getKey(void)
{
	return key;
}

char getLongkey(void)
{
	return longkey;
}

void prvKeyboardTask(void *pvParameters)
{
	if (pvParameters) {
		return;
	}
	k1s = BKP_ReadBackupRegister(BKP_DR2);
	sendMsg(0, 0, key1states[k1s]);
	keyboardGpioSet();
	keyboardMain();
}
