#ifndef FIFO_H
#define FIFO_H

#include "FreeRTOS.h"
#include "semphr.h"

typedef struct {
	char* fifo;
	int fifoSize;
	int wp;
	int rp;
	xSemaphoreHandle semaphore;
} RingBuffer;

int getFifoContentSize(RingBuffer* rb);
void writeFifo(RingBuffer* rb, const char* sourceBuffer, int sourceBufferLen);
int readFifo(RingBuffer* rb, char* targetBuffer);

#endif // FIFO_H
